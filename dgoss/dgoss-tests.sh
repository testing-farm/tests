#!/bin/bash
set -exo pipefail

systemctl start docker

ENTRYPOINT=${ENTRYPOINT:-bash}

if [ -n "$PACKAGES" ]; then
    dnf install -y $PACKAGES
fi

if [ -n "$DOCKER_AUTH_CONFIG_BASE64" ]; then
    mkdir -p $HOME/.docker
    base64 -d <<< "$DOCKER_AUTH_CONFIG_BASE64" > $HOME/.docker/config.json
fi

if [ -n "$VAULT_SECRET_ID" ]; then
    VAULT_ARGS="-e VAULT_SECRET_ID=$VAULT_SECRET_ID"
fi

if [ -n "$USER" ]; then
    USER_ARG="--user $USER"
fi

curl -fsSL https://goss.rocks/install | sh
docker pull $IMAGE

for GOSS in ${GOSS_YAML/,/ }; do
    curl -Lo goss.yaml $GOSS
    dgoss run $VAULT_ARGS --rm -it --entrypoint $ENTRYPOINT $USER_ARG $IMAGE $COMMAND_ARGS
done
