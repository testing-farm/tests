#!/bin/sh -eux

GREEN='\033[0;32m'
NC='\033[0m'
RED='\033[0;31m'

info() { echo -e "${GREEN}$@${NC}"; }
error() { echo -e "${RED}$@${NC}"; exit 1; }

# sanity
for cmd in curl tft-admin; do
    if ! command -v $cmd &>/dev/null; then
        info "NOTE: This scripts expects it runs in Testing Farm CnC image."
        error "ERROR: Command $cmd not found"
    fi
done

for var in CLOUD COMPOSES_SCRIPT; do
    if [ -z "$(eval echo \$$var)" ]; then
        error "ERROR: environemnt variable $var is not defined."
    fi
done

tft-admin cloud set $CLOUD

# test
tmpdir=$(mktemp -d)
trap "sync; rm -rf $tmpdir" EXIT
pushd $tmpdir

info "Getting list of composes"
eval "curl -sL $COMPOSES_URL | $(base64 -d <<< $COMPOSES_SCRIPT)" | tee composes

if [ ! -s composes ]; then
    error "Failed to download and filter out composes, composes file is empty"
fi

# Choose new line as a delimiter for loop
IFS=$'\n'

FAILED=0
for COMPOSE in $(cat composes); do
    # This is a workaround to check TF-BUILD-* composes
    # The problem is that in variables-compose it doesn't have arch suffixes
    # TODO: make it properly once variables-composes will have arches flags
    if [ $CLOUD == 'arr-aws' ] && $(echo $COMPOSE | grep -q 'TF-BUILD'); then
        for arch in x86_64 aarch64; do
            if ! tft-admin cloud compose sync --cloud-compose "${COMPOSE}-${arch}"; then
                FAILED=$((++FAILED))
            fi
        done
        continue
    fi
    # It is expected to miss composes in labs in beaker
    if [ $CLOUD == 'bci-beaker' ]; then
        # Ignore Fedora distros
        if $(echo $COMPOSE | grep -q 'Fedora'); then
            continue
        fi
        if ! tft-admin cloud compose sync --skip-missing-labs --cloud-compose "$COMPOSE"; then
            FAILED=$((++FAILED))
        fi
        continue
    fi
    if ! tft-admin cloud compose sync --cloud-compose "$COMPOSE"; then
        FAILED=$((++FAILED))
    fi
done

if [ -n "$FAILED" -a $FAILED -ne 0 ]; then
    error "Could not find $FAILED cloud composes on '$CLOUD' cloud"
fi
