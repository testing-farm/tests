# This script expects on stdin a list of packages in the following format:
# <NAME> <EPOCH> <VERSION> <RELEASE> <ARCH>
# For each package listed it prints the latest (highest) NEVRA

from __future__ import print_function

import functools
import itertools
import sys

import rpm


def cmp(a, b):
    # ignore name and arch when comparing
    return rpm.labelCompare(tuple(a[1:4]), tuple(b[1:4]))


def main():
    # load the list of packages from stdin, sort by name
    packages = sorted(
        (pkg.split() for pkg in sys.stdin.read().splitlines()), key=lambda x: x[0]
    )
    # group the list by name and find the highest EVR in each group
    for _, group in itertools.groupby(packages, key=lambda x: x[0]):
        n, e, v, r, a = max(group, key=functools.cmp_to_key(cmp))
        print("{0}-{1}:{2}-{3}.{4}".format(n, e, v, r, a))


if __name__ == "__main__":
    main()
